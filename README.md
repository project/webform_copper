## INTRODUCTION ##

Author and maintainer: **Andrii Sakhaniuk**
 * Drupal: https://www.drupal.org/u/nnevill

Sponsored by: **Creative Propulsion Labs**
 * Drupal : https://www.drupal.org/creative-propulsion-labs
 * Website : https://www.creativepl.com/

The module provides Webform handler which allows to create Copper leads based on webform submission data.

## INSTALLATION ##

See https://www.drupal.org/documentation/install/modules-themes/modules-8
for instructions on how to install or update Drupal modules.

## CONFIGURATION ##

* Get Copper API key. For this login into your Copper account. Go to Settings->Integrations->API Keys.
* Open /admin/config/services/webform-copper on your Drupal installation and enter email and API key there.
* Open "Emails/Handlers" section of your webform and add "Copper handler".
* Provide mapping between webform's fields and Copper fields. Possible copper fields could be found here https://developer.copper.com/leads/create-a-new-lead.html. **Important** – field "name" is required for Copper.

### PERMISSIONS ###

The module permission 'administer webform copper' can be configured under
/admin/people/permissions.

### API HOOKS ###

Module supports only 4 webforn field types: email, textfield, textarea, select. I hope it will be enough for most common cases. But you can add support for other fields using
`hook_webform_copper_supported_fields(){}`..

Also it's possible to alter data passing to Copper via
`hook_webform_copper_data_alter(array &$copper_fields, WebformSubmissionInterface $webform_submission){}`.

Check webform_copper.api.php for more info.

### IMPORTANT ###
You have to enable Leads on Copper UI side. To do so go to "Settings" -> open(if not opened) "Customize" fieldset -> "Lead Statuses" and click "Enable Leads" checkbox there. After that "Leads" link appears in your UI and integration will work.
