<?php

namespace Drupal\webform_copper\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Webform Copper settings for this site.
 */
class WebformCopperAdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webform_copper_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['webform_copper.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('webform_copper.settings');

    $form['email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email'),
      '#required' => TRUE,
      '#default_value' => $config->get('email'),
      '#description' => $this->t('Email address of Copper token owner.'),
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Copper API Key'),
      '#required' => TRUE,
      '#default_value' => $config->get('api_key'),
      '#description' => $this->t('The API key of your Copper account.'),
    ];
    $form['force_details'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send not mapped fields in "details" Copper field.'),
      '#default_value' => $config->get('force_details'),
      '#description' => $this->t('"Details" Copper field is a textarea which could contain all additional data passed through webform.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('webform_copper.settings');
    $config
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('email', $form_state->getValue('email'))
      ->set('force_details', $form_state->getValue('force_details'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
