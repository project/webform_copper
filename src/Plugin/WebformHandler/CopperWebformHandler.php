<?php

namespace Drupal\webform_copper\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Create a Copper Leads from Webform submission.
 *
 * @WebformHandler(
 *   id = "copper_handler",
 *   label = @Translation("Copper"),
 *   category = @Translation("Web services"),
 *   description = @Translation("Creates a Lead in Copper when a form is submitted."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class CopperWebformHandler extends WebformHandlerBase {

  /**
   * Coppers API URL.
   */
  const COOPER_API_URL = 'https://api.copper.com/developer_api/v1/leads';

  /**
   * The Guzzle HTTP Client service.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * A logger instance for CloudFlare.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->httpClient = $container->get('http_client');
    $instance->moduleHandler = $container->get('module_handler');
    $instance->configFactory = $container->get('config.factory');
    $instance->logger = $container->get('logger.factory')->get('webform_copper');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    return [
      '#markup' => $this->t('Copper integration'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // Getting all webform fields.
    $fields = $this->webform->getElementsDecodedAndFlattened();

    // Getting all supported fields.
    $supported_fields = $this->getSupportedFields();

    foreach ($fields as $field_key => $field) {
      if (in_array($field['#type'], array_keys($supported_fields))) {
        $form[$field_key] = [
          '#title' => $field['#title'],
          '#type' => 'textfield',
          '#default_value' => !empty($this->configuration['copper_mapping'][$field_key]) ? $this->configuration['copper_mapping'][$field_key] : '',
          '#attributes' => ['placeholder' => $this->t('Enter Copper field mapping. E.g. "name", "company_name" etc.')],
        ];
      }
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    foreach ($form_state->getValues() as $key => $value) {
      $this->configuration['copper_mapping'][$key] = $value;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    $conf = $this->configFactory->get('webform_copper.settings');
    $api_key = $conf->get('api_key');
    $email = $conf->get('email');
    if (!$api_key || !$email) {
      $this->logger->notice("Copper integration hasn't been configured correctly. Check %here.", ['%here' => Link::createFromRoute('here', 'webform_copper.admin')->toString()]);
      return;
    }
    $is_completed = ($webform_submission->getState() == WebformSubmissionInterface::STATE_COMPLETED);
    if ($is_completed) {
      try {
        $this->httpClient
          ->post(self::COOPER_API_URL, [
            'body' => json_encode($this->getCopperFields($webform_submission)),
            'headers' => [
              'Content-Type' => 'application/json',
              'X-PW-AccessToken' => $api_key,
              'X-PW-Application' => 'developer_api',
              'X-PW-UserEmail' => $email,
            ],
          ]);
      }
      catch (RequestException $e) {
        // Log error message.
        $context = [
          '@form' => $this->getWebform()->label(),
          'link' => $this->getWebform()->toLink($this->t('Edit handlers'), 'handlers')->toString(),
          '@error' => $e->getMessage(),
        ];
        $this->logger->error('@form webform failed to create the Copper Lead. @error', $context);
      }
    }
  }

  /**
   * Returns list of Copper-friendly webform fields.
   *
   * @return array
   *   List with webform fields which have Copper support.
   */
  public function getSupportedFields() {
    // Getting all supported fields.
    $supported_fields = $this->moduleHandler->invokeAll('webform_copper_supported_fields');
    return $supported_fields ?: [];
  }

  /**
   * Returns list of Copper fields from webform submission using fields mapping.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   Webform submission.
   *
   * @return array
   *   Mapped Copper fields ready to be sent.
   */
  public function getCopperFields(WebformSubmissionInterface $webform_submission) {
    $conf = $this->configFactory->get('webform_copper.settings');

    $supported_fields = $this->getSupportedFields();

    $webform = $webform_submission->getWebform();

    // Fields to be sent in a Copper request.
    $copper_fields = [];
    // Array with fields values which don't have mapping.
    $copper_fields['details'] = [];
    foreach ($webform_submission->getData() as $field => $value) {
      $webform_field = $webform->getElement($field);
      $webform_field_type = $webform_field['#type'];

      // Skip not supported field types.
      if (!isset($supported_fields[$webform_field_type])) {
        continue;
      }

      // Getting Copper-friendly field value.
      $copper_field_value = call_user_func($supported_fields[$webform_field_type], $webform_submission, $field);

      // Contains actual Copper field machine name or empty string.
      $copper_field_name = $this->configuration['copper_mapping'][$field];

      // Apply mapping defined in handler configuration.
      if (!empty($copper_field_name)) {
        // Some fields have specific structure so require specific handling.
        switch ($field) {
          case 'email':
            $copper_fields[$copper_field_name] = [
              'email' => $copper_field_value,
              'category' => 'work',
            ];
            break;

          case 'telephone':
            $copper_fields[$copper_field_name] = [
              [
                'number' => $copper_field_value,
                'category' => 'work',
              ],
            ];
            break;

          case 'details':
            $copper_fields[$copper_field_name][] = "{$webform_field['#title']}: {$copper_field_value}";
            break;

          default:
            if ($copper_field_name == 'details') {
              $copper_fields[$copper_field_name][] = "{$webform_field['#title']}: {$copper_field_value}";
            }
            else {
              $copper_fields[$copper_field_name] = $copper_field_value;
            }
            break;

        }
      }
      else {
        // Add field value to Copper 'details' field if this is allowed.
        if ($conf->get('force_details')) {
          $copper_fields['details'][] = "{$webform_field['#title']}: {$copper_field_value}";
        }
      }
    }

    // Allow other modules to change data before sending to Copper.
    $this->moduleHandler->invokeAll('webform_copper_data_alter', [
      &$copper_fields,
      $webform_submission,
    ]);

    // Populating details field with all values from not mapped fields.
    if (!empty($copper_fields['details']) && is_array($copper_fields['details'])) {
      $copper_fields['details'] = implode("\n", $copper_fields['details']);
    }
    return $copper_fields;
  }

}
