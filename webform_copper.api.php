<?php

/**
 * @file
 * Hooks related to Webform Copper module.
 */

use Drupal\webform\WebformSubmissionInterface;

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Defines if webform fields are supported and provides callbacks for values.
 *
 * @return array
 *   An associative with supported webform fields as array keys and callbacks
 *   for getting values for those keys as array values.
 *
 * @see webform_copper_webform_copper_supported_fields()
 * @see webform_copper_get_common_field_value()
 */
function hook_webform_copper_supported_fields() {
  // Adds support for checkbox webform element.
  // Callback receives webform submission object and webform field name.
  return [
    'checkbox' => 'my_callback_for_checkbox_field',
  ];
}

/**
 * Allows other modules to alter Copper data before sending to Copper.
 *
 * @param array $copper_fields
 *   Associative with Copper fields data containing e.g.:
 *   - name: Lead's name.
 *   - email: Associative array with lead's email containing:
 *     - email: Lead's email string.
 *     - category: Email category – hardcoded string 'work'.
 *   - details: Array with non-mapped and custom fields.
 * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
 *   Webform submission.
 */
function hook_webform_copper_data_alter(array &$copper_fields, WebformSubmissionInterface $webform_submission) {

}

/**
 * @} End of "addtogroup hooks".
 */
